#!/bin/sh
. ../config/openbeelab.conf

DATE=$( date +%Y-%m-%d )
TIME=$( date +%H:%M )


###################################
# get datas from external sensor
###################################
EXTPRESSURE=`~/.local/bin/read_bme280 --i2c-address $EXTSENS --pressure`
EXTHUMIDITY=`~/.local/bin/read_bme280 --i2c-address $EXTSENS --humidity`
EXTTEMPERATURE=`~/.local/bin/read_bme280 --i2c-address $EXTSENS --temperature`

#modify a bit format for datas (remove space and unity)
EXTPRESSURE=`echo $EXTPRESSURE | sed 's/ *//g' | sed 's/hPa//g'`
EXTHUMIDITY=`echo $EXTHUMIDITY | sed 's/ *//g' | sed 's/%//g'`
EXTTEMPERATURE=`echo $EXTTEMPERATURE | sed 's/ *//g' | sed 's/C//g'`


###################################
# get datas from internal sensor
###################################
INTPRESSURE=`~/.local/bin/read_bme280 --i2c-address $INTSENS --pressure`
INTHUMIDITY=`~/.local/bin/read_bme280 --i2c-address $INTSENS --humidity`
INTTEMPERATURE=`~/.local/bin/read_bme280 --i2c-address $INTSENS --temperature`

#modify a bit format for datas (remove space and unity)
INTPRESSURE=`echo $INTPRESSURE | sed 's/ *//g' | sed 's/hPa//g'`
INTHUMIDITY=`echo $INTHUMIDITY | sed 's/ *//g' | sed 's/%//g'`
INTTEMPERATURE=`echo $INTTEMPERATURE | sed 's/ *//g' | sed 's/C//g'`


# inserting in database
sqlite3 $BASE/sqlite/openbeelab.db "insert into BME280PRESSURE values ('$DATE $TIME','$INTPRESSURE','$EXTPRESSURE')";
sqlite3 $BASE/sqlite/openbeelab.db "insert into BME280TEMPERATURE values ('$DATE $TIME','$INTTEMPERATURE','$EXTTEMPERATURE')";
sqlite3 $BASE/sqlite/openbeelab.db "insert into BME280HUMIDITY values ('$DATE $TIME','$INTHUMIDITY','$EXTHUMIDITY')";

