#!/bin/sh 
######################
# Get datas from SQL
######################
sqlite3 ../sqlite/openbeelab.db "SELECT DATE,INTERNAL,EXTERNAL FROM BME280PRESSURE ORDER BY DATE DESC limit 120;" > pressure.gnuplot
sqlite3 ../sqlite/openbeelab.db "SELECT DATE,INTERNAL,EXTERNAL FROM BME280TEMPERATURE ORDER BY DATE DESC limit 120;" > temperature.gnuplot
sqlite3 ../sqlite/openbeelab.db "SELECT DATE,INTERNAL,EXTERNAL FROM BME280HUMIDITY ORDER BY DATE DESC limit 120;" > humidity.gnuplot


######################
# GRaphs datas from SQL
######################
cat ../config/pressure_gnuplot.conf | gnuplot
cat ../config/temperature_gnuplot.conf | gnuplot
cat ../config/humidity_gnuplot.conf | gnuplot
